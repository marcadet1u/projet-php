<?php
namespace mywishlist\vue;

session_start();

class VueCreateur{


	public function affichage_connecté($res){

		$html = '
		<form id="f1" method="post">
					  ' . $res . '

					<div>
						<label for="titre">Titre de la liste :</label>
						<input type="text" id="titre" name="titre" />
					</div>

					<div>
						<label for="desc">Description de la liste de cadeaux :</label>
						<textarea id="desc" name = "desc"></textarea>
					</div>

					<div>
						<label for="date">Date d\'expiration :</label>
						<input type="date" id="date" name="date" />
					</div>

					<div>
						<button type="submit" name="valider" value="valid_reserv">Creer la liste</button>
					</div>

		</form>
		';
		return($html);
	}

	public function affichage_deco(){
		$html = 'Connectez-vous ou inscrivez-vous pour pouvoir creer une nouvelle liste !';
		return($html);
	}



	// methode affichage general
	public function render($html){


	if(isset($_SESSION['email'])){
		$content = $this->affichage_connecté($html);
	}else {
		$content = $this->affichage_deco();
	}

	$html = <<<END
	<!DOCTYPE html>
		<html>
		<head>
			<meta charset="utf-8" />
			<link rel="stylesheet" type="text/css" href="../web/css/createur.css" />
			<title>My WishList</title>
			<link rel="shortcut icon" href="web/img/logo.ico">

					<div class="header">
					</div>

						<nav>
								<ul>
									<div class="topnav">
										<li><a href="http://localhost/projet/projet-php/mywishlist/index.php">Accueil</a></li>
										<li><a href="http://localhost/projet/projet-php/mywishlist/index.php/connexion">Connexion</a></li>
										<li><a href="http://localhost/projet/projet-php/mywishlist/index.php/inscription">Inscription</a></li>
										<li><a href="http://localhost/projet/projet-php/mywishlist/index.php/profil">Mon profil</a></li>
										<li><a href="http://localhost/projet/projet-php/mywishlist/index.php/newliste">Creer une liste</a></li>
									</div>
								</ul>
						</nav>
	  </head>
		<body>

				$content



			<footer>
			</footer>

		</body>
		</html>

END;

	echo $html;
	}

}
