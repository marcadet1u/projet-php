<?php

namespace mywishlist\controler;
use \mywishlist\models\Liste;
use \mywishlist\models\Utilisateur;
use \mywishlist\vue\VueAccueil;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

/** connection base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
*/

session_start();

define('RESERV',8);

class ControleurReservation{

  public function reserver(){
    // on vérifie que les champs sont bien remplis
    if(  !empty($_POST['num']) AND !empty($_POST['msg']) ){
          if(isset($_SESSION['email'])){
                // récupération des variables du formulaire
                $num = htmlspecialchars($_POST['num']);
                $msg = htmlspecialchars($_POST['msg']);

                  // verifier si l'item est deja reservé
                  $item = \mywishlist\models\Item::find($num);
                  if($item['reservation'] == 'non'){


                      $item = \mywishlist\models\Item::find($num);
                      $item->reservation = 'oui';
                      $item->save();
                      $vue = new VueAccueil();
                      $vue->render(RESERV);
                  }else{
                    $vue = new VueAccueil();
                    $vue->render(DEJA_RESERV);
                  }

              }else{
                $vue = new VueAccueil();
                $vue->render(ERR);
              }
          }else{
            if(  !empty($_POST['num']) AND !empty($_POST['nom']) AND !empty($_POST['descr']) AND !empty($_POST['prix']) ){
              if(isset($_SESSION['email'])){
                $num = htmlspecialchars($_POST['num']);
                $nom = htmlspecialchars($_POST['nom']);
                $desc = htmlspecialchars($_POST['descr']);
                $prix = htmlspecialchars($_POST['prix']);

                $item = new \mywishlist\models\Item();
                $item->liste_id = $num;
                $item->nom = $nom;
                $item->descr = $desc;
                $item->img = "";
                $item->url = "";
                $item->tarif = $prix;
                $item->reservation = 'non';
                $item->save();

                $vue = new VueAccueil();
                $vue->render(CREE);

              }
            }
          }

    }




    public function ajouter(){

      if(  !empty($_POST['num']) AND !empty($_POST['nom']) AND !empty($_POST['descr']) AND !empty($_POST['prix']) ){
        if(isset($_SESSION['email'])){
          $num = htmlspecialchars($_POST['num']);
          $nom = htmlspecialchars($_POST['nom']);
          $desc = htmlspecialchars($_POST['descr']);
          $prix = htmlspecialchars($_POST['prix']);

          $item = new \mywishlist\models\Item();
          $item->liste_id = $num;
          $item->nom = $nom;
          $item->descr = $desc;
          $item->img = "";
          $item->url = "";
          $item->tarif = $prix;
          $item->reservation = 'non';
          $item->save();

          $vue = new VueProfil();
          $vue->render();

        }
      }


    }










  }
