<?php

namespace mywishlist\controler;
use \mywishlist\models\Utilisateur;
use \mywishlist\vue\VueConnexion;

session_start();

class ControleurDeconnexion{

  public function deconnexion(){

    $_SESSION = array();

    session_destroy();
    $vue = new VueConnexion();
  	$select = null;
  	$vue->render($select);
  }



}
