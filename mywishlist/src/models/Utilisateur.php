<?php

namespace mywishlist\models;

class Utilisateur extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'utilisateur';
	protected $primaryKey = 'id_utilisateur';
	public $timestamps = false;

	public function roles(){
		return $this->belongsToMany('\mywishlist\models\Role');
	}



}
