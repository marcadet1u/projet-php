<?php

namespace mywishlist\controler;
use \mywishlist\models\Liste;
use \mywishlist\models\Utilisateur;
use \mywishlist\vue\VueProfil;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

// connection base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

session_start();
define('CREE',1);
class ControleurCreateur{

  public function creer_liste(){

    // on vérifie que les champs sont bien remplis
    if(  !empty($_POST['titre']) AND !empty($_POST['desc'])  AND !empty($_POST['date'])){
          if(isset($_SESSION['email'])){
            $email = $_SESSION['email'];
            // récupération des variables du formulaire
            $titre = htmlspecialchars($_POST['titre']);
            $desc = htmlspecialchars($_POST['desc']);
            $expi = $_POST['date'];
            $user = \mywishlist\models\Utilisateur::where($email);
                  if(isset($user)){
                    $user_id = \mywishlist\models\Utilisateur::select( 'id_util')
                                    ->where( 'email', 'like', $email)
                                    ->first() ;
                    preg_match_all('#[0-9]+#',$user_id,$extract);
                    $nombre = $extract[0][0];

                    $liste = new Liste();
                    $liste->user_id = $nombre;
                    $liste->titre = $titre;
                    $liste->description = $desc;
                    $liste->expiration = $expi;
                    $liste->token = bin2hex(random_bytes(32));
                    $liste->save();

                    // creer vue liste / num créé
                    //\mywishlist\controler\Authentication::deconnexion();
                    $html = 'Votre liste a bien été créé !';
                    $vue = new \mywishlist\vue\VueCreateur();
                  	$vue->render($html);
                }else{
                  echo "erreur1";
                }
          }else{
            echo "variable session inexistante";
          }



    }
  }



}
