<?php

namespace mywishlist\controler;
namespace mywishlist\vue;
use \mywishlist\models\Utilisateur;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

// connection base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

class VueProfil{

	public function afficher_null(){
		$html = '
				<p>
					Connectez vous pour pouvoir modifier votre profil !
				</p>

				<a href="http://localhost/projet/projet-php/mywishlist/index.php/connexion">Connexion</a>
		';
		return($html);
	}

	public function afficher_Info(){
		$mail = $_SESSION['email'];
		$util = Utilisateur::where('email', '=', $mail)->first();
		$html = '<p>
							Votre profil :
						</p>' .
						'<p>' . 'Email : ' . $_SESSION['email'] . '</p>' . '<br>' .
						'<p>' . 'Nom : ' . $util->nom . '</p>'  . '<br>' .
						'<p>' . 'Prenom : ' . $util->prenom . '</p>'  . '<br>' .
						'<p>' . 'Mot de passe : ' . $util->passpassword . '</p>'  . '<br>';
		return($html);
	}

	// methode affichage general
	public function render(){

		if(isset($_SESSION['email'])){
			$html = $this->afficher_Info();
		}else{
			$html = $this->afficher_null();
		}

	$html = <<<END
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="../web/css/connexion.css" />
		<title>My WishList</title>
		<link rel="shortcut icon" href="web/img/logo.ico">

		<div class="header">
		</div>

			<nav>
					<ul>
						<div class="topnav">
							<li><a href="http://localhost/projet/projet-php/mywishlist/index.php">Accueil</a></li>
							<li><a href="http://localhost/projet/projet-php/mywishlist/index.php/connexion">Connexion</a></li>
							<li><a href="http://localhost/projet/projet-php/mywishlist/index.php/inscription">Inscription</a></li>
							<li><a href="http://localhost/projet/projet-php/mywishlist/index.php/profil">Mon profil</a></li>
							<li><a href="http://localhost/projet/projet-php/mywishlist/index.php/newliste">Creer une liste</a></li>
						</div>
					</ul>
			</nav>
	</head>
    <body>

			<p>
        $html
      </p>

			<p>
				<a href ="#">Editer mon profil</a>
				<a href="http://localhost/projet/projet-php/mywishlist/index.php/deconnexion">Deconnexion</a>
			</p>

	  <footer>
	  </footer>

	</body>
	</html>

END;

	echo $html;
	}

}
